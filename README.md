# Teste Java Texo

HTTP GET em:

----
1. http://localhost:8080/movies/{year}
2. http://localhost:8080/movies/yearsmoreonewinners
3. http://localhost:8080/movies/studiosorderwinns
4. http://localhost:8080/movies/awardsinterval
----

HTTP DELETE em:

----
1. http://localhost:8080/movies/{id}
----

No caso do endpoint inexistente o serviço responderá com uma representação JSON com uma mensagem de código padrão.

Exemplo de resposta padrão no caso de endpoint inexistente:

----
{
"codreturn": "00400",
"timestamp": 1538677176588,
"status": "400",
"message": "Bad Request – A solicitação não pôde ser entendida pelo servidor devido à sintaxe malformada.",
"path": "http://localhost:8080/xpto"
}
----
== Endpoins

Obtém os vencedores, informando um ano.
----
http://localhost:8080/movies/{year}
----

Exemplo de resposta:

[source,json]
----
{
"movies": [
{
"id": 194,
"year": "2017",
"title": "Baywatch",
"studios": [
{
"name": "Paramount Pictures"
}
],
"producers": [
{
"name": "Ivan Reitman"
},
{
"name": "Michael Berk"
},
{
"name": "Douglas Schwartz"
},
{
"name": "Gregory J. Bonann and Beau Flynn"
}
],
"winner": false
}

    "returnStatus": {
        "codreturn": "00200",
        "timestamp": 1538763477531,
        "status": "200",
        "message": "OK –  Obtenção com sucesso do(s) dado(s).",
        "path": "/movies/1999"
    }        
}
----

Obtém os anos que tiveram mais de um vencedor.
----
http://localhost:8080/movies/yearsmoreonewinners
----

Exemplo de resposta:

[source,json]
----
{
"years": [
{
"year": "1986",
"winnerCount": 2
},
{
"year": "2015",
"winnerCount": 2
},
{
"year": "1990",
"winnerCount": 2
}
],
"returnStatus": {
"codreturn": "00200",
"timestamp": 1538678413946,
"status": "200",
"message": "OK –  Obtenção com sucesso do(s) dado(s).",
"path": "/movies/yearsmoreonewinners"
}
}
----

Obtém a lista de estúdios, ordenada pelo número de premiações.

----
http://localhost:8080/movies/studiosorderwinns
----

Exemplo de resposta:

[source,json]
----
{
"studios": [
{
"name": "Paramount Pictures",
"winCount": 6
},
{
"name": "Columbia Pictures",
"winCount": 5
},
{
"name": "Warner Bros.",
"winCount": 5
},
{
"name": "20th Century Fox",
"winCount": 4
},
{
"name": "MGM",
"winCount": 3
},
{
"name": "Hollywood Pictures",
"winCount": 2
},
{
"name": "Universal Studios",
"winCount": 2
},
{
"name": "Summit Entertainment",
"winCount": 1
}
"returnStatus": {
"codreturn": "00200",
"timestamp": 1538763588718,
"status": "200",
"message": "OK –  Obtenção com sucesso do(s) dado(s).",
"path": "/movies/studiosorderwinns"
}        
}
----

Obtém o produtor com maior intervalo entre dois prêmios, e o que obteve dois prêmios mais rápido.

----
http://localhost:8080/movies/awardsinterval
----

Exemplo de resposta:

[source,json]
----
{
"min": [
{
"producer": "Wyck Godfrey, Stephenie Meyer and Karen Rosenfelt",
"interval": 1,
"previousWin": 2011,
"followingWin": 2012
}
],
"max": [
{
"producer": "Bo Derek",
"interval": 6,
"previousWin": 1984,
"followingWin": 1990
}
],
"returnStatus": {
"codreturn": "00200",
"timestamp": 1538679244773,
"status": "200",
"message": "OK –  Obtenção com sucesso do(s) dado(s).",
"path": "/movies/awardsinterval"
}
}
----

Exclui um filme. Não permite excluir vencedores.

----
http://localhost:8080/movies/{id} //HTTP DELETE
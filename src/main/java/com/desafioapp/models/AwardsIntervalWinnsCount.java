package com.desafioapp.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AwardsIntervalWinnsCount {
	private String year;
	private int winnerCount;
}

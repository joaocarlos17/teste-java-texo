package com.desafioapp.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudiosOrderWinns {
	private String name;
	private int winCount;
}
